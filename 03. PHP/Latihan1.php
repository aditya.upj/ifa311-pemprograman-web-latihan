<!DOCTYPE html>
<html lang="en">
<head>
           <meta charset="UTF-8">
           <meta name="viewport" content="width=device-width, initial-scale=1.0">
           <title>Document</title>
</head>
<body>
           <?php
                      $namaDepan = "Ade";
                      $namaBelakang = "Aditya";
                      $namaLengkap = $namaDepan." ".$namaBelakang;

                      echo "Salam kenal semua, saya ".$namaLengkap."<br>";
           ?>

           <!-- Latihan membuat data seperti berikut 

                      Biodata
                      Nama Lengkap          : Adenia Cintya
                      Umur                  : 24 tahun

                      #adanya statement pengecekan umur
                      # jika lebih dari 17 tahun maka cetak "Anda sudah dewasa ya Adenia"
                      # jika kurang dari sama dengan 17 maka cetak "Anda masih muda ya Adenia"
                      # jika lebih dari 30 tahun maka cetak "Anda tua ya Adenia. Hahaha"
           -->

           <?php
                      $namaDepan = "Adenia";
                      $namaBelakang = "Cintya";
                      $namaLengkap = $namaDepan." ".$namaBelakang;
                      $umur = 24;

                      echo "<br>Biodata";
                      echo "<br>Nama Lengkap\t: ".$namaLengkap;
                      echo "<br>Umur\t\t: ".$umur." tahun<br>";
                      
                      if ($umur <= 17) {
                                 echo "Anda masih muda ya ".$namaDepan;
                      }
                      else if ($umur > 17 && $umur < 30) {
                                 echo "Anda masih muda ya ".$namaDepan;
                      }
                      else{
                                 echo "Anda sudah tua ya ".$namaDepan."<br>System : Hahaha";
                      }
           ?>
</body>
</html>
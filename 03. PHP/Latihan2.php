<!DOCTYPE html>
<html lang="en">
<head>
           <meta charset="UTF-8">
           <meta name="viewport" content="width=device-width, initial-scale=1.0">
           <title>Document</title>
</head>
<body>
           <!-- Latihan membuat data seperti berikut 

                      Biodata
                      Nama Lengkap          : Adenia Cintya
                      Umur                  : 24 tahun

                      #adanya statement pengecekan umur
                      # jika lebih dari 17 tahun maka cetak "Anda sudah dewasa ya Adenia"
                      # jika kurang dari sama dengan 17 maka cetak "Anda masih muda ya Adenia"
                      # jika lebih dari 30 tahun maka cetak "Anda tua ya Adenia. Hahaha"
           -->

           <?php
                      $namaDepan = 'Adenia';
                      $namaBelakang = 'Cintya';
                      $namaLengkap = $namaDepan . ' ' . $namaBelakang;
                      $umur = 24;
           ?>

           <!-- Menggunakan CSS -->
           <div class="container">
                      <h2>Biodata</h2>

                      <table>
                                 <thead>
                                            <tr>
                                                       <th>Nama Lengkap</th>
                                                       <th><?php echo $namaLengkap; ?></th>
                                            </tr>           
                                 </thead>
                                 <tbody>
                                            <tr>
                                                       <td>Umur</td>
                                                       <td><?php echo $umur; ?></td>
                                            </tr>
                                 </tbody>
                      </table>
                      
                      <?php if ($umur <= 17) {
                          echo 'Anda masih muda ya ' . $namaDepan;
                      } elseif ($umur > 17 && $umur < 30) {
                          echo 'Anda masih muda ya ' . $namaDepan;
                      } else {
                          echo 'Anda sudah tua ya ' .
                              $namaDepan .
                              '<br>System : Hahaha';
                      } ?>

           </div>
</body>
</html>
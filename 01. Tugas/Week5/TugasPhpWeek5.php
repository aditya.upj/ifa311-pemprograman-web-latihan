<!DOCTYPE html>
<html lang="en">
<head>
           <meta charset="UTF-8">
           <meta name="viewport" content="width=device-width, initial-scale=1.0">
           <title>Tugas ke 4</title>

           <!-- referensi CSS add here ya -->
           <!-- this is reference for bootstrap -->
           <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
                 integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous" />
</head>
<body>
<div class="col-md-9 right-cell">
    <div class="heading"> <div class="head-6"> <h4>Pendaftaran Online </h4> </div> </div> <div class="row">
        <div class="col-md-12 abjad-list">
            <form class="form" method="post" enctype="multipart/form-data" action="" data-parsley-validate="" novalidate="">

                <div class="row">
                    <div class="form-group">
                        <div class="col-md-12 marbot-10">
                            <label>Jalur Penerimaan Mahasiswa Baru *</label>
                            <select required="" class="form-control" name="pmbjalur" id="">
                                <option value="" style="font-weight: bold">--- Reguler ---</option>
                                <option value="TA 2022/2023 Rata-Rata Rapor">TA 2022/2023 Rata-Rata Rapor</option>
                                <option value="TA 2022/2023 Peringkat Kelas">TA 2022/2023 Peringkat Kelas</option>
                                <option value="TA 2022/2023 Ujian Saringan Masuk (USM)">TA 2022/2023 Ujian Saringan Masuk (USM)</option>
                                <option value="" style="font-weight: bold">--- Beasiswa Prestasi Jaya ---</option>
                                <option value="TA 2022/2023 Akademik">TA 2022/2023 Akademik</option>
                                <option value="TA 2022/2023 Olahraga">TA 2022/2023 Olahraga</option>
                                <option value="TA 2022/2023 Seni dan Budaya">TA 2022/2023 Seni dan Budaya</option>
                                <option value="TA 2022/2023 Sains">TA 2022/2023 Sains</option>
                                <option value="" style="font-weight: bold">--- Beasiswa UPJ untuk Negeri ---</option>
                                <option value="TA 2022/2023 ASAK">TA 2022/2023 ASAK</option>
                                <option value="TA 2022/2023 OSIS/MPK">TA 2022/2023 OSIS/MPK</option>
                                <option value="" style="font-weight: bold">--- Beasiswa Desain Produk ---</option>
                                <option value="TA 2022/2023 Desain Produk">TA 2022/2023 Desain Produk</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Nama Calon Mahasiswa (Sesuai Akte Kelahiran) *</label> <input type="text" value="" data-parsley-required-message="Ini harus diisi.." required="" class="form-control" name="pmbname" id="" placeholder="Silahkan masukkan nama lengkap...">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label>Tempat &amp; Tanggal Lahir *</label>
                            <input type="text" value="" required="" class="form-control" name="pmbbirth" id="" placeholder="Silahkan masukkan tempat, tanggal/bulan/tahun lahir...">
                        </div>
                        <div class="col-md-6">
                            <label>NIK (Nomor Induk Kependudukan KTP / KK) *</label>
                            <input type="text" value="" required="" class="form-control" name="pmbnik" id="" placeholder="Silahkan masukkan Nomor Induk Kependudukan...">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label>Jenis Kelamin *</label>
                            <select required="" class="form-control" name="pmbjkel" id="">
                                <option value="">--- Pilih ---</option>
                                <option value="Pria">Pria</option>
                                <option value="Wanita">Wanita</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Agama *</label>
                            <select required="" class="form-control" name="pmbagama" id="">
                                <option value="">--- Pilih ---</option>
                                <option value="Islam">Islam</option>
                                <option value="Katolik">Katolik</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Budha">Budha</option>
                                <option value="Lainnya">Lainnya</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Status *</label>
                            <select required="" class="form-control" name="pmbstatusnikah" id="">
                                <option value="">--- Pilih ---</option>
                                <option value="Belum Menikah">Belum Menikah</option>
                                <option value="Menikah">Menikah</option>
                                <option value="Janda">Janda</option>
                                <option value="Duda">Duda</option>
                            </select>
                        </div>
                    </div>
                </div> <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Alamat *</label>
                            <textarea required="" rows="3" class="form-control" name="pmbaddress" id="" placeholder="Silahkan masukkan alamat lengkap..."></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label>Kode Pos *</label>
                            <input type="text" value="" required="" class="form-control" name="pmbpostcode" id="" placeholder="Silahkan masukkan kodepos...">
                        </div>
                        <div class="col-md-4">
                            <label>No Handphone *</label>
                            <input type="text" value="" required="" class="form-control" name="pmbhp" id="" placeholder="Silahkan masukkan no. handphone...">
                        </div>
                        <div class="col-md-4">
                            <label>Email *</label>
                            <input type="email" value="" data-parsley-trigger="change" required="" class="form-control" name="pmbemail" id="" placeholder="Silahkan masukkan email...">
                        </div>
                    </div>
                </div>
                <br>
                <h2 class="col-lg-12 clearfix">DATA SEKOLAH ASAL CALON MAHASISWA</h2>
                <br>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label>Asal Sekolah (SMA/SMK/MA) *</label>
                            <input type="text" value="" required="" class="form-control" name="pmblulusan" id="" placeholder="Silahkan masukkan asal sekolah...">
                        </div>
                        <div class="col-md-4">
                            <label>Kota Sekolah *</label>
                            <input type="text" value="" required="" class="form-control" name="pmbkota" id="" placeholder="Silahkan masukkan kota asal sekolah...">
                        </div>
                        <div class="col-md-4">
                            <label>Provinsi Sekolah *</label>
                            <select required="" class="locationMultiple form-control" name="pmbwilayah" id="">
                                <option value="">--- Pilih ---</option>
                                <option value="Aceh">Aceh</option>
                                <option value="Bali">Bali</option>
                                <option value="Banten">Banten</option>
                                <option value="Bengkulu">Bengkulu</option>
                                <option value="Bogor">Bogor</option>
                                <option value="Depok">Depok</option>
                                <option value="DI Yogyakarta">DI Yogyakarta</option>
                                <option value="Gorontalo">Gorontalo</option>
                                <option value="Jakarta Barat">Jakarta Barat</option>
                                <option value="Jakarta Pusat">Jakarta Pusat</option>
                                <option value="Jakarta Selatan">Jakarta Selatan</option>
                                <option value="Jakarta Timur">Jakarta Timur</option>
                                <option value="Jakarta Utara">Jakarta Utara</option>
                                <option value="Jambi">Jambi</option>
                                <option value="Jawa Barat">Jawa Barat</option>
                                <option value="Jawa Tengah">Jawa Tengah</option>
                                <option value="Jawa Timur">Jawa Timur</option>
                                <option value="Kalimantan Barat">Kalimantan Barat</option>
                                <option value="Kalimantan Selatan">Kalimantan Selatan</option>
                                <option value="Kalimantan Tengah">Kalimantan Tengah</option>
                                <option value="Kalimantan Timur">Kalimantan Timur</option>
                                <option value="Kalimantan Utara">Kalimantan Utara</option>
                                <option value="Kepulauan Bangka Belitung">Kepulauan Bangka Belitung</option>
                                <option value="Kepulauan Riau">Kepulauan Riau</option>
                                <option value="Lampung">Lampung</option>
                                <option value="Maluku">Maluku</option>
                                <option value="Maluku Utara">Maluku Utara</option>
                                <option value="Nusa Tenggara Barat">Nusa Tenggara Barat</option>
                                <option value="Nusa Tenggara Timur">Nusa Tenggara Timur</option>
                                <option value="Papua">Papua</option>
                                <option value="Papua Barat">Papua Barat</option>
                                <option value="Riau">Riau</option>
                                <option value="Sulawesi Barat">Sulawesi Barat</option>
                                <option value="Sulawesi Selatan">Sulawesi Selatan</option>
                                <option value="Sulawesi Tengah">Sulawesi Tengah</option>
                                <option value="Sulawesi Tenggara">Sulawesi Tenggara</option>
                                <option value="Sulawesi Utara">Sulawesi Utara</option>
                                <option value="Sumatera Barat">Sumatera Barat</option>
                                <option value="Sumatera Selatan">Sumatera Selatan</option>
                                <option value="Sumatera Utara">Sumatera Utara</option>
                                <option value="Tangerang">Tangerang</option>
                                <option value="Tangerang Selatan">Tangerang Selatan</option>
                                <option value="Bekasi">Bekasi</option>
                                <option value="Luar Negri">Luar Negri</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label>Jurusan Sekolah *</label>
                            <input type="text" value="" required="" class="form-control" name="pmbjurusan" id="" placeholder="Silahkan masukkan jurusan sekolah...">
                        </div>
                        <div class="col-md-4">
                            <label>Tahun Lulus *</label>
                            <input type="text" value="" required="" class="form-control" name="pmbgraduateyear" id="" placeholder="Silahkan masukkan tahun lulus...">
                        </div>
                        <div class="col-md-4">
                            <label>Biaya kuliah dibiayai oleh *</label>
                            <select required="" class="form-control" name="pmbbiaya" id="">
                                <option value="">--- Pilih ---</option>
                                <option value="Beasiswa">Beasiswa</option>
                                <option value="Ikatan Dinas">Ikatan Dinas</option>
                                <option value="Orangtua">Orangtua</option>
                                <option value="Wali">Wali</option>
                                <option value="Sendiri">Sendiri</option>
                                <option value="Lain-lain">Lain-lain</option>
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <h2 class="col-lg-12 clearfix">DATA ORANG TUA CALON MAHASISWA</h2>
                <br>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label>Nama Ayah *</label>
                            <input type="text" value="" required="" class="form-control" name="pmbdadyname" id="" placeholder="Silahkan masukkan nama ayah...">
                        </div>
                        <div class="col-md-6">
                            <label>Nama Ibu *</label>
                            <input type="text" value="" required="" class="form-control" name="pmbmammyname" id="" placeholder="Silahkan masukkan nama ibu...">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Alamat *</label>
                            <textarea required="" rows="3" class="form-control" name="pmbparentaddress" id="" placeholder="Silahkan masukkan alamat lengkap..."></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label>Kode Pos *</label>
                            <input type="text" value="" required="" class="form-control" name="pmbparentpostcode" id="" placeholder="Silahkan masukkan kodepos...">
                        </div>
                        <div class="col-md-6">
                            <label>No. Handphone *</label>
                            <input type="text" value="" required="" class="form-control" name="pmbparenthp" id="" placeholder="Silahkan masukkan no. handphone...">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label>Pendidikan Terakhir Ayah *</label>
                            <select required="" class="form-control" name="pmbdadylaststudy" id="">
                                <option value="">--- Pilih ---</option>
                                <option value="A">A - Tidak Tamat SD</option>
                                <option value="B">B - Tamat SD</option>
                                <option value="C">C - Tamat SMP</option>
                                <option value="D">D - Tamat SMA</option>
                                <option value="E">E - Diploma ( D1 - D2 )</option>
                                <option value="F">F - Sarjana ( D3 )</option>
                                <option value="G">G - Sarjana ( D4 - S1 )</option>
                                <option value="H">H - Pascasarjana ( S2 )</option>
                                <option value="I">I - Doktor ( S3 )</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Pendidikan Terakhir Ibu *</label>
                            <select required="" class="form-control" name="pmbmammylaststudy" id="">
                                <option value="">--- Pilih ---</option>
                                <option value="A">A - Tidak Tamat SD</option>
                                <option value="B">B - Tamat SD</option>
                                <option value="C">C - Tamat SMP</option>
                                <option value="D">D - Tamat SMA</option>
                                <option value="E">E - Diploma ( D1 - D2 )</option>
                                <option value="F">F - Sarjana ( D3 )</option>
                                <option value="G">G - Sarjana ( D4 - S1 )</option>
                                <option value="H">H - Pascasarjana ( S2 )</option>
                                <option value="I">I - Doktor ( S3 )</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label>Status Ayah *</label>
                            <select required="" class="form-control" name="pmbdadystatus" id="">
                                <option value="">--- Pilih ---</option>
                                <option value="Masih Hidup">Masih Hidup</option>
                                <option value="Sudah Meninggal">Sudah Meninggal</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Status Ibu *</label>
                            <select required="" class="form-control" name="pmbmammystatus" id="">
                                <option value="">--- Pilih ---</option>
                                <option value="Masih Hidup">Masih Hidup</option>
                                <option value="Sudah Meninggal">Sudah Meninggal</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label>Pekerjaan Ayah *</label>
                            <select required="" class="form-control" name="pmbdadyjob" id="">
                                <option value="">--- Pilih ---</option>
                                <option value="Pegawai Negeri">Pegawai Negeri</option>
                                <option value="TNI/POLRI">TNI/POLRI</option>
                                <option value="Pegawai Swasta">Pegawai Swasta</option>
                                <option value="Wiraswasta">Wiraswasta</option>
                                <option value="Tidak Bekerja">Tidak Bekerja</option>
                                <option value="Pensiun">Pensiun</option>
                                <option value="Lainnya">Lainnya</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Pekerjaan Ibu *</label>
                            <select required="" class="form-control" name="pmbmammyjob" id="">
                                <option value="">--- Pilih ---</option>
                                <option value="Pegawai Negeri">Pegawai Negeri</option>
                                <option value="TNI/POLRI">TNI/POLRI</option>
                                <option value="Pegawai Swasta">Pegawai Swasta</option>
                                <option value="Wiraswasta">Wiraswasta</option>
                                <option value="Tidak Bekerja">Tidak Bekerja</option>
                                <option value="Pensiun">Pensiun</option>
                                <option value="Lainnya">Lainnya</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label>Penghasilan Ayah *</label>
                            <select required="" class="form-control" name="pmbdadysalary" id="">
                                <option value="">--- Pilih ---</option>
                                <option value="< Rp 1.000.000">&lt; Rp 1.000.000</option>
                                <option value="Rp 1.000.000 - Rp 5.000.000">Rp 1.000.000 - Rp 5.000.000</option>
                                <option value="Rp 5.000.000 - Rp 10.000.000">Rp 5.000.000 - Rp 10.000.000</option>
                                <option value="> Rp 10.000.000">&gt; Rp 10.000.000</option>
                                <option value="Pensiun">Pensiun</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Penghasilan Ibu *</label>
                            <select required="" class="form-control" name="pmbmammysalary" id="">
                                <option value="">--- Pilih ---</option>
                                <option value="< Rp 1.000.000">&lt; Rp 1.000.000</option>
                                <option value="Rp 1.000.000 - Rp 5.000.000">Rp 1.000.000 - Rp 5.000.000</option>
                                <option value="Rp 5.000.000 - Rp 10.000.000">Rp 5.000.000 - Rp 10.000.000</option>
                                <option value="> Rp 10.000.000">&gt; Rp 10.000.000</option>
                                <option value="Pensiun">Pensiun</option>
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <h2 class="col-lg-12 clearfix">PEMINATAN PROGRAM STUDI</h2>
                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label>Pilihan Program Studi *</label>
                            <select required="" class="form-control" name="pmbprogramstudy" id="pmbprogramstudy">
                                <option value="">--- Pilih ---</option>
                                <option value="Akuntansi">Akuntansi</option>
                                <option value="Manajemen">Manajemen</option>
                                <option value="Psikologi">Psikologi</option>
                                <option value="Ilmu Komunikasi">Ilmu Komunikasi</option>
                                <option value="Desain Produk">Desain Produk</option>
                                <option value="Desain Komunikasi Visual">Desain Komunikasi Visual</option>
                                <option value="Informatika">Informatika</option>
                                <option value="Sistem Informasi">Sistem Informasi</option>
                                <option value="Teknik Sipil">Teknik Sipil</option>
                                <option value="Arsitektur">Arsitektur</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <label>Informasi UPJ diperoleh dari : *</label>
                        <span id="pmbknowwe_error"></span>
                    </div>
                    <div class="col-lg-4">
                        <input name="pmbknowwe" value="Website" type="radio" required="" data-parsley-errors-container="#pmbknowwe_error" data-parsley-multiple="pmbknowwe">&nbsp;&nbsp;Website<br>
                        <input name="pmbknowwe" value="Instagram" type="radio" data-parsley-multiple="pmbknowwe">&nbsp;&nbsp;Instagram<br>
                        <input name="pmbknowwe" value="Facebook" type="radio" data-parsley-multiple="pmbknowwe">&nbsp;&nbsp;Facebook<br>
                        <input name="pmbknowwe" value="Brosur dan Flyer UPJ" type="radio" data-parsley-multiple="pmbknowwe">&nbsp;&nbsp;Brosur dan Flyer UPJ<br>
                        <input name="pmbknowwe" value="WA Blast" type="radio" data-parsley-multiple="pmbknowwe">&nbsp;&nbsp;WA Blast<br>
                    </div>
                    <div class="col-lg-4">
                        <input name="pmbknowwe" value="Sekolah" type="radio" data-parsley-multiple="pmbknowwe">&nbsp;&nbsp;Sekolah<br>
                        <input name="pmbknowwe" value="Event Education Expo" type="radio" data-parsley-multiple="pmbknowwe">&nbsp;&nbsp;Event Education Expo<br>
                        <input name="pmbknowwe" value="Keluarga/Teman" type="radio" data-parsley-multiple="pmbknowwe">&nbsp;&nbsp;Keluarga/Teman<br>
                        <input name="pmbknowwe" value="E-mail Blast" type="radio" data-parsley-multiple="pmbknowwe">&nbsp;&nbsp;E-mail Blast<br>
                        <div class="row" style="margin-top: 0;">
                            <div class="col-xs-5">
                                <input name="pmbknowwe" value="Lainnya" type="radio" data-parsley-multiple="pmbknowwe">&nbsp;&nbsp;Lainnya
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-4">
                    </div>
                    <div class="col-lg-4">
                        <label>Direkomendasikan oleh :</label>
                    </div><br><br>
                    <div class="form-group">
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-4">
                            <label>PRODI/UNIT</label><br>
                            <input name="pmbnimrecomend" value="Karyawan UPJ" type="radio" data-parsley-multiple="pmbnimrecomend">&nbsp;&nbsp;Karyawan UPJ<br>
                            <input name="pmbnimrecomend" value="Mahasiswa UPJ" type="radio" data-parsley-multiple="pmbnimrecomend">&nbsp;&nbsp;Mahasiswa UPJ<br>
                            <input name="pmbnimrecomend" value="Guru BK/Pihak Sekolah" type="radio" data-parsley-multiple="pmbnimrecomend">&nbsp;&nbsp;Guru BK/Pihak Sekolah<br><br>
                            <label>Nama Perekomendasi &amp; No. HP</label>
                            <input type="text" value="" class="form-control" name="pmbnoperecomend" id="" placeholder="Nama Perekomendasi &amp; No.Handphone">
                        </div>
                    </div>
                </div>
                <h2>Ujian Saringan Masuk</h2>
                <div class="row">
                    <div class="col-md-12">
                        <font>
                            USM (Ujian Saringan Masuk):  Jadwal dan link akan diberikan kemudian
                            setelah proses awal pendaftaran dinyatakan memenuhi persyaratan
                        </font>
                    </div>
                </div>
                <div class="row">
                </div>
                <div class="row">
                    <div class="col-lg-12" style="clear:both; margin:8px 0;">
                        <div class="col-lg-3 col-lg-offset-3">
                            <input type="hidden" name="csrf_token" value="ILAb7IW9X43k-Hr-YBSLl_l02C5mQvz31F43t8FOtS0">
                            <button value="Submit" name="submit" class="btn btn-primary" type="submit">Submit</button>
                            <button value="Reset" name="B2" class="btn btn-danger" type="reset">Reset</button>
                        </div>
                    </div>
                </div> <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger" id="contactError">
                            * harus diisi
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
</body>
</html>